package com.example.imageviewer;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.toolbox.ImageLoader.ImageCache;

/** Wrap an LRU bitmap cache to be used with the Volley network library. */
public class BitmapCache implements ImageCache
{
	private final BitmapLruCache bitmapCache;

	public BitmapCache(Context context)
	{
		bitmapCache = new BitmapLruCache();

		// Get max available VM memory, exceeding this amount will throw an
		// OutOfMemory exception.
//		final int maxMemory = (int)Runtime.getRuntime().maxMemory();
//
//		bitmapCache = new BitmapLruCache.Builder(context)
//			.setMemoryCacheEnabled(true)
//			.setDiskCacheEnabled(true)
//			.setMemoryCacheMaxSize(maxMemory / 4)
//			.build();
	}

	@Override
	public Bitmap getBitmap(String url)
	{
//		BitmapDrawable d = bitmapCache.get(url);
//		return (d != null) ? d.getBitmap() : null;
		return bitmapCache.get(url);
	}

	@Override
	public void putBitmap(String url, Bitmap bitmap)
	{
		bitmapCache.put(url, bitmap);
	}

	public static class BitmapLruCache extends LruCache<String, Bitmap> implements ImageCache {
	    public static int getDefaultLruCacheSize() {
	        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
	        final int cacheSize = maxMemory / 8;

	        return cacheSize;
	    }

	    public BitmapLruCache() {
	        this(getDefaultLruCacheSize());
	    }

	    public BitmapLruCache(int sizeInKiloBytes) {
	        super(sizeInKiloBytes);
	    }

	    @Override
	    protected int sizeOf(String key, Bitmap value) {
	        return value.getRowBytes() * value.getHeight() / 1024;
	    }

	    @Override
	    public Bitmap getBitmap(String url) {
	        return get(url);
	    }

	    @Override
	    public void putBitmap(String url, Bitmap bitmap) {
	        put(url, bitmap);
	    }
	}
	
}
