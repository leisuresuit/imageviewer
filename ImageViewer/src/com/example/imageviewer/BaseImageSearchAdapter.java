package com.example.imageviewer;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.imageviewer.ImageGridActivity.ImageAdapter;
import com.example.imageviewer.ui.MTImageView;

public abstract class BaseImageSearchAdapter<T, R> extends ImageAdapter
{
	private static String TAG_IMAGE = "image";
	
	private final RequestQueue requestQueue;
	protected final ArrayList<R> searchResult;
	private final OnSearchResultListener onSearchResultListener;

	public BaseImageSearchAdapter(Context context, OnSearchResultListener onSearchResultListener,
		MTImageView.MTImageViewListener onImageChangedListener, View.OnClickListener onClickListener)
	{
		super(context, onImageChangedListener, onClickListener);

		requestQueue = Volley.newRequestQueue(context);
		searchResult = createSearchResultList();
		this.onSearchResultListener = onSearchResultListener;
	}

	@Override
	public void search(String query)
	{
		requestQueue.cancelAll(TAG_IMAGE);
		searchResult.clear();
		notifyDataSetChanged();
		search(query, null);
		
	}		

	/**
	 * @param query	The search query.
	 * @param data Optional data (provided by a subclass).
	 */
	protected void search(String query, Object data)
	{
		Request<T> request = createRequest(query, data);
		if (request != null)
		{
			request.setTag(TAG_IMAGE);
			requestQueue.add(request);
		}
	}
	
	protected abstract ArrayList<R> createSearchResultList();
	protected abstract Request<T> createRequest(String query, Object data);

	@Override
	public int getCount() {
		return searchResult.size();
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	protected void notifySearchDone()
	{
		if (onSearchResultListener != null)
		{
			onSearchResultListener.onSearchDone();
		}
	}

	public interface OnSearchResultListener
	{
		public void onSearchDone();
		public void onSearchError(String message);
	}
	
	public class VolleyErrorResponseListener implements Response.ErrorListener
	{
		@Override
		public void onErrorResponse(VolleyError error)
		{
			if (onSearchResultListener != null)
			{
				onSearchResultListener.onSearchError(error.getMessage());
			}
		}
	}
}
