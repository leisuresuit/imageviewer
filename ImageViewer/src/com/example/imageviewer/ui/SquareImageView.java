package com.example.imageviewer.ui;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Maintains a square aspect ratio.
 */
public class SquareImageView extends MTImageView
{

	public SquareImageView(Context context)
	{
		super(context);
	}

	public SquareImageView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public SquareImageView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		int w = getMeasuredWidth();
		setMeasuredDimension(w, w);

	}

}
