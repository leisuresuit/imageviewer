package com.example.imageviewer.flickr;

import java.util.Date;

public class FlickrImageSearchResponse
{
	public String title;
	public String link;
	public String desription;
	public Date modified;
	public String generator;
	public FlickrImage[] items;
	
	public static class FlickrImage
	{
		public String title;
		public String link;
		Media media;
		Date date_taken;
		String description;
		Date published;
		String author;
		String author_id;
		String tags;
	}
	
	public static class Media
	{
		String m;
	}
}
