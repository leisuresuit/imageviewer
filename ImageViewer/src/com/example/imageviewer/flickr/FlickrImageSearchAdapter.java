package com.example.imageviewer.flickr;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import android.content.Context;
import android.view.View.OnClickListener;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.example.gson.GsonRequest;
import com.example.imageviewer.BaseImageSearchAdapter;
import com.example.imageviewer.ImageData;
import com.example.imageviewer.flickr.FlickrImageSearchResponse.FlickrImage;
import com.example.imageviewer.ui.MTImageView.MTImageViewListener;
import com.google.gson.JsonSyntaxException;

public class FlickrImageSearchAdapter extends BaseImageSearchAdapter<FlickrImageSearchResponse, FlickrImage>
{
	private static final String SEARCH_URL = "https://api.flickr.com/services/feeds/photos_public.gne?tags=%1$s&format=json";
	
	public FlickrImageSearchAdapter(Context context, OnSearchResultListener onSearchResultListener,
		MTImageViewListener onImageChangedListener, OnClickListener onClickListener)
	{
		super(context, onSearchResultListener, onImageChangedListener, onClickListener);
	}

	@Override
	protected ArrayList<FlickrImage> createSearchResultList() {
		return new ArrayList<FlickrImage>();
	}

	@Override
	protected Request<FlickrImageSearchResponse> createRequest(String query, Object data) {
		String url;
		try {
			url = String.format(SEARCH_URL, URLEncoder.encode(query, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			return null;
		}
		
		GsonRequest<FlickrImageSearchResponse> request = new GsonRequest<FlickrImageSearchResponse>(
			Request.Method.GET,
			url, FlickrImageSearchResponse.class, null,
			new Response.Listener<FlickrImageSearchResponse>()
			{
				@Override
				public void onResponse(FlickrImageSearchResponse response)
				{
					if (response.items != null && response.items.length > 0)
					{
						int i = 0;
						for (FlickrImage image : response.items)
						{
							searchResult.add(image);
							if (++i == 64)
							{
								break;
							}
						}

						notifySearchDone();
						notifyDataSetChanged();
					}
					else
					{
						notifySearchDone();
					}
				}
			},
			new VolleyErrorResponseListener()) {
			
			@Override
			protected Response<FlickrImageSearchResponse> parseNetworkResponse(NetworkResponse response)
			{
				try
				{
					String json = new String(
						response.data, HttpHeaderParser.parseCharset(response.headers));
					
					// Strip out non-standard JSON strings from Flickr
					json = json.substring("jsonFlickrFeed(".length(), json.length()-1);

					return Response.success(
						gson.fromJson(json, FlickrImageSearchResponse.class), HttpHeaderParser.parseCacheHeaders(response));
				}
				catch (UnsupportedEncodingException e)
				{
					return Response.error(new ParseError(e));
				}
				catch (JsonSyntaxException e)
				{
					return Response.error(new ParseError(e));
				}
			}

		};
		
		return request;
	}

	@Override
	public ImageData getItem(int position) {
		FlickrImage result = searchResult.get(position);
		return new ImageData(result.title + "<p/>" + result.author, result.media.m, result.media.m);
	}

}
