/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.imageviewer;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.imageviewer.flickr.FlickrImageSearchAdapter;
import com.example.imageviewer.google.GoogleImageSearchAdapter;
import com.example.imageviewer.ui.MTImageView;
import com.nhaarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;

public class ImageGridActivity extends Activity implements BaseImageSearchAdapter.OnSearchResultListener, MTImageView.MTImageViewListener, View.OnClickListener
{
	/*pkg*/static float sAnimatorScale = 1;

	private View loading;
	private AbsListView grid;
	private ImageAdapter imageAdapter;

	private String query;
	
	/*pkg*/RequestQueue requestQueue;
	/*pkg*/static ImageLoader imageLoader;

	private enum SearchProvider
	{
		GOOGLE,
		FLICKR;
	}
	
	private SearchProvider searchProvider = SearchProvider.GOOGLE;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		requestQueue = Volley.newRequestQueue(this);
		BitmapCache imageCache = new BitmapCache(this);
		imageLoader = new ImageLoader(requestQueue, imageCache);

		initUi();

		initImageAdapter(new GoogleImageSearchAdapter(this, this, this, this));

		handleIntent(getIntent());
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		
		ListAdapter adapter = grid.getAdapter();
		initUi();
		grid.setAdapter(adapter);
	}

	private void initUi()
	{
		setContentView(R.layout.image_grid);

		grid = (AbsListView)findViewById(R.id.grid);
		loading = findViewById(R.id.loading);
	}

	private void initImageAdapter(ImageAdapter adapter)
	{
		imageAdapter = adapter;
		SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(imageAdapter);
		swingBottomInAnimationAdapter.setAbsListView(grid);
		swingBottomInAnimationAdapter.setInitialDelayMillis(300);
		grid.setAdapter(swingBottomInAnimationAdapter);
	}
	
	@Override
	protected void onNewIntent(Intent intent)
	{
		handleIntent(intent);
	}

	private void handleIntent(Intent intent)
	{
		if (Intent.ACTION_SEARCH.equals(intent.getAction()))
		{
			query = intent.getStringExtra(SearchManager.QUERY);
			startSearch();
		}
	}

	private void startSearch()
	{
		if (!TextUtils.isEmpty(query))
		{
			loading.setVisibility(View.VISIBLE);
			grid.setEmptyView(null);
			imageAdapter.search(query);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.options_menu, menu);

		// Associate searchable configuration with the SearchView
		SearchManager searchManager =
			(SearchManager)getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView =
			(SearchView)menu.findItem(R.id.search).getActionView();
		searchView.setSearchableInfo(
			searchManager.getSearchableInfo(getComponentName()));
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.google).setChecked(searchProvider == SearchProvider.GOOGLE);
		menu.findItem(R.id.flickr).setChecked(searchProvider == SearchProvider.FLICKR);
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId())
		{
		case R.id.google:
			if (searchProvider != SearchProvider.GOOGLE)
			{
				searchProvider = SearchProvider.GOOGLE;
				initImageAdapter(new GoogleImageSearchAdapter(this, this, this, this));
				startSearch();
			}
			return true;
		case R.id.flickr:
			if (searchProvider != SearchProvider.FLICKR)
			{
				searchProvider = SearchProvider.FLICKR;
				initImageAdapter(new FlickrImageSearchAdapter(this, this, this, this));
				startSearch();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public static abstract class ImageAdapter extends BaseAdapter
	{
		private final Context context;
		private final MTImageView.MTImageViewListener onImageChangedListener;
		private final View.OnClickListener onClickListener;

		public ImageAdapter(Context context,
			MTImageView.MTImageViewListener onImageChangedListener, View.OnClickListener onClickListener)
		{
			this.context = context;
			this.onImageChangedListener = onImageChangedListener;
			this.onClickListener = onClickListener;
		}
		
		protected Context getContext()
		{
			return context;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder;
			MTImageView iv;
			View v = convertView;
			if (v == null)
			{
				v = View.inflate(context, R.layout.image, null);
				holder = new ViewHolder();
				holder.image = (MTImageView)v.findViewById(R.id.image);
				holder.image.setOnClickListener(onClickListener);
				holder.loading = (ProgressBar)v.findViewById(R.id.loading);
				v.setTag(holder);
			}
			else
			{
				holder = (ViewHolder)v.getTag();
			}

			iv = holder.image;

			ImageData data = getItem(position);
			if (data != null && (holder.data == null || !holder.data.thumbnailUrl.equals(data.thumbnailUrl)))
			{
				holder.data = data;
				iv.setTag(holder);

				if (holder.data.thumbnailUrl != null)
				{
					iv.setListener(onImageChangedListener);
					iv.setImageUrl(holder.data.thumbnailUrl, ImageGridActivity.imageLoader);
				}
			}

			return v;
		}

		public abstract void search(String query);

		@Override
		public abstract ImageData getItem(int position);

	}

	private static class ViewHolder
	{
		ImageData data;
		MTImageView image;
		ProgressBar loading;
	}
	
	@Override
	public void onSearchDone() {
		loading.setVisibility(View.GONE);
		grid.setEmptyView(findViewById(android.R.id.empty));
	}
	
	@Override
	public void onSearchError(String message) {
		loading.setVisibility(View.GONE);
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onImageChanged(MTImageView view, Bitmap bmp, boolean isImmediate)
	{
		if (bmp != null)
		{
			ViewHolder holder = (ViewHolder)view.getTag();
			holder.loading.setVisibility(View.GONE);
			view.setListener(null);

			if (!isImmediate)
			{
				Animation fadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
				fadeIn.setFillAfter(false);
				view.startAnimation(fadeIn);
			}
		}
	}

	@Override
	public void onErrorResponse(MTImageView view, VolleyError error)
	{
		ViewHolder holder = (ViewHolder)view.getTag();
		holder.loading.setVisibility(View.GONE);
	}

	@Override
	public void onClick(View v)
	{
		// Interesting data to pass across are the thumbnail size/location, the
		// resourceId of the source bitmap, the picture description, and the
		// orientation (to avoid returning back to an obsolete configuration if
		// the device rotates again in the meantime)
		int[] screenLocation = new int[2];
		v.getLocationOnScreen(screenLocation);
		ViewHolder holder = (ViewHolder)v.getTag();
		ImageData data = holder.data;
		Intent subActivity = new Intent(this, ImageDetailsActivity.class);
		int orientation = getResources().getConfiguration().orientation;
		subActivity.
			putExtra(ImageDetailsActivity.EXTRA_ORIENTATION, orientation).
			putExtra(ImageDetailsActivity.EXTRA_LEFT, screenLocation[0]).
			putExtra(ImageDetailsActivity.EXTRA_TOP, screenLocation[1]).
			putExtra(ImageDetailsActivity.EXTRA_WIDTH, v.getWidth()).
			putExtra(ImageDetailsActivity.EXTRA_HEIGHT, v.getHeight()).
			putExtra(ImageDetailsActivity.EXTRA_THUMBNAIL_URL, data.thumbnailUrl).
			putExtra(ImageDetailsActivity.EXTRA_URL, data.url).
			putExtra(ImageDetailsActivity.EXTRA_DESCRIPTION, data.description);
		startActivity(subActivity);

		// Override transitions: we don't want the normal window animation in addition
		// to our custom one
		overridePendingTransition(0, 0);
	}

}
