/**
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.imageviewer.ui;

import uk.co.senab.bitmapcache.CacheableImageView;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;

/**
 * Handles fetching an image from a URL as well as the life-cycle of the
 * associated request.
 * <p>
 * Ported from <code>com.android.volley.toolbox.NetworkImageView</code>
 * to extend from <code>CacheableImageView</code> instead of <code>ImageView</code>.
 */
public class NetworkCacheableImageView extends CacheableImageView implements ImageListener {
    /** The URL of the network image to load */
    private String mUrl;

    /**
     * Resource ID of the image to be used as a placeholder until the network image is loaded.
     */
    private int mDefaultImageId;

    /**
     * Resource ID of the image to be used if the network response fails.
     */
    private int mErrorImageId;

    /** Local copy of the ImageLoader. */
    private ImageLoader mImageLoader;

    /** Current ImageContainer. (either in-flight or finished) */
    private ImageContainer mImageContainer;

    /** Maximum image width, <code>0</code> if none. */
    private int mMaxWidth;

    /** Maximum image height, <code>0</code> if none. */
    private int mMaxHeight;

    public NetworkCacheableImageView(Context context) {
        this(context, null);
    }

    public NetworkCacheableImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NetworkCacheableImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Sets URL of the image that should be loaded into this view. Note that calling this will
     * immediately either set the cached image (if available) or the default image specified by
     * {@link NetworkCacheableImageView#setDefaultImageResId(int)} on the view.
     *
     * NOTE: If applicable, {@link NetworkCacheableImageView#setDefaultImageResId(int)} and
     * {@link NetworkCacheableImageView#setErrorImageResId(int)} should be called prior to calling
     * this function.
     *
     * @param url The URL that should be loaded into this ImageView.
     * @param imageLoader ImageLoader that will be used to make the request.
     */
    public void setImageUrl(String url, ImageLoader imageLoader) {
        mUrl = url;
        mImageLoader = imageLoader;
        // The URL has potentially changed. See if we need to load it.
        loadImageIfNecessary();
    }

    public String getImageUrl() {
    	return mUrl;
    }

    /**
     * Sets URL of the image that should be loaded into this view. Note that calling this will
     * immediately either set the cached image (if available) or the default image specified by
     * {@link NetworkCacheableImageView#setDefaultImageResId(int)} on the view.
     *
     * NOTE: If applicable, {@link NetworkCacheableImageView#setDefaultImageResId(int)} and
     * {@link NetworkCacheableImageView#setErrorImageResId(int)} should be called prior to calling
     * this function.
     *
     * @param url The URL that should be loaded into this ImageView.
     * @param imageLoader ImageLoader that will be used to make the request.
     * @param maxWidth The maximum width of the returned image.
     * @param maxHeight The maximum height of the returned image.
     */
    public void setImageUrl(String url, ImageLoader imageLoader, int maxWidth, int maxHeight) {
        mUrl = url;
        mImageLoader = imageLoader;
        mMaxWidth = maxWidth;
        mMaxHeight = maxHeight;

        // The URL has potentially changed. See if we need to load it.
        loadImageIfNecessary();
    }

    /**
     * Sets the default image resource ID to be used for this view until the attempt to load it
     * completes.
     */
    public void setDefaultImageResId(int defaultImage) {
        mDefaultImageId = defaultImage;
    }

    /**
     * Sets the error image resource ID to be used for this view in the event that the image
     * requested fails to load.
     */
    public void setErrorImageResId(int errorImage) {
        mErrorImageId = errorImage;
    }

    /**
     * Loads the image for the view if it isn't already loaded.
     */
    private void loadImageIfNecessary() {
        int width = getWidth();
        int height = getHeight();

        // if the URL to be loaded in this view is empty, cancel any old requests and clear the
        // currently loaded image.
        if (TextUtils.isEmpty(mUrl)) {
            if (mImageContainer != null) {
                mImageContainer.cancelRequest();
                mImageContainer = null;
            }
            setImageBitmap(null);
            return;
        }

        // if there was an old request in this view, check if it needs to be canceled.
        if (mImageContainer != null && mImageContainer.getRequestUrl() != null) {
            if (mImageContainer.getRequestUrl().equals(mUrl)) {
                // if the request is from the same URL, return.
                return;
            } else {
                // if there is a pre-existing request, cancel it if it's fetching a different URL.
                mImageContainer.cancelRequest();
                setImageBitmap(null);
            }
        }

        // The pre-existing content of this view didn't match the current URL. Load the new image
        // from the network.
        // Limit the dimension of the image.
        int imageWidth = (mMaxWidth > 0) ? Math.min(width, mMaxWidth) : width;
        int imageHeight = (mMaxHeight > 0) ? Math.min(width, mMaxHeight) : height;

        try {
	        // The pre-existing content of this view didn't match the current URL. Load the new image
	        // from the network.
            ImageContainer newContainer = mImageLoader.get(mUrl, this, imageWidth, imageHeight);

            // update the ImageContainer to be the new bitmap container.
            mImageContainer = newContainer;

        } catch (Exception e) {
        	// Can get an exception for a malformed url.
        	Log.e("NetworkCacheableImageView", "loadImageIfNecessary: Failed to get image for: " + mUrl, e);
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (mErrorImageId != 0) {
            setImageResource(mErrorImageId);
        }
    }

    @Override
    public void onResponse(ImageContainer response, boolean isImmediate) {
    	if (response.getBitmap() != null) {
            setImageBitmap(response.getBitmap());
        } else if (mDefaultImageId != 0) {
            setImageResource(mDefaultImageId);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        if (mImageContainer != null) {
            // If the view was bound to an image request, cancel it and clear
            // out the image from the view.
            mImageContainer.cancelRequest();
            setImageBitmap(null);
            // also clear out the container so we can reload the image if necessary.
            mImageContainer = null;
        }
        super.onDetachedFromWindow();
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        invalidate();
    }

}
