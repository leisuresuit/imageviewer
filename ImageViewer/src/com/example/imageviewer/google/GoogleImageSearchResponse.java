package com.example.imageviewer.google;

public class GoogleImageSearchResponse
{
	public ResponseData responseData;
	public int responseStatus;

	public static class ResponseData
	{
		public GoogleImage[] results;
	}

	public static class GoogleImage
	{
		public int width;
		public int height;
		public String imageId;
		public int tbWidth;
		public int tbHeight;
		public String unescapedUrl;
		public String url;
		public String visibleUrl;
		public String titleNoFormatting;
		public String originalContextUrl;
		public String contentNoFormatting;
		public String tbUrl;
	}

}
