package com.example.imageviewer;

public class ImageData
{
	/*pkg*/final CharSequence description;
	/*pkg*/final String thumbnailUrl;
	/*pkg*/final String url;

	public ImageData(CharSequence description, String thumbnailUrl, String url)
	{
		this.description = description;
		this.thumbnailUrl = thumbnailUrl;
		this.url = url;
	}
}
