package com.example.imageviewer.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader.ImageContainer;

/**
 * Common image view (with fade-in effect).
 */
public class MTImageView extends NetworkCacheableImageView
{

	public interface MTImageViewListener
	{

		/**
		 * @param view The image view.
		 * @param bmp The new image.
		 * @param isImmediate <code>true</code> if the image was loaded from the memory cache.
		 */
		void onImageChanged(MTImageView view, Bitmap bmp, boolean isImmediate);

		/**
		 * Handles an error fetching an image.
		 * 
		 * @param view The image view.
		 * @param error The error.
		 */
		void onErrorResponse(MTImageView view, VolleyError error);

	}

	private MTImageViewListener mListener;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public MTImageView(Context context)
	{
		super(context);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
		{
			setLayerType(View.LAYER_TYPE_HARDWARE, null);
		}
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public MTImageView(Context context, AttributeSet attrs)
	{
		super(context, attrs);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
		{
			setLayerType(View.LAYER_TYPE_HARDWARE, null);
		}
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public MTImageView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
		{
			setLayerType(View.LAYER_TYPE_HARDWARE, null);
		}
	}

	public void setListener(MTImageViewListener listener)
	{
		mListener = listener;
	}

	@Override
	public void onErrorResponse(VolleyError error)
	{
		super.onErrorResponse(error);

		if (mListener != null)
		{
			mListener.onErrorResponse(this, error);
		}
	}

	@Override
	public void onResponse(ImageContainer response, boolean isImmediate)
	{
		super.onResponse(response, isImmediate);

		if (mListener != null)
		{
			Bitmap bmp = response.getBitmap();
			mListener.onImageChanged(this, bmp, isImmediate);
		}

	}

	@Override
	protected void onDetachedFromWindow()
	{
		super.onDetachedFromWindow();

		setTag(null);
		setListener(null);
	}

}
