package com.example.imageviewer.google;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import android.content.Context;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.example.gson.GsonRequest;
import com.example.imageviewer.BaseImageSearchAdapter;
import com.example.imageviewer.ImageData;
import com.example.imageviewer.google.GoogleImageSearchResponse.GoogleImage;
import com.example.imageviewer.ui.MTImageView;

public class GoogleImageSearchAdapter extends BaseImageSearchAdapter<GoogleImageSearchResponse, GoogleImage>
{
	private static final String SEARCH_URL = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&rsz=8&start=%1$d&q=%2$s";
	
	public GoogleImageSearchAdapter(Context context, OnSearchResultListener onSearchResultListener,
		MTImageView.MTImageViewListener onImageChangedListener, View.OnClickListener onClickListener)
	{
		super(context, onSearchResultListener, onImageChangedListener, onClickListener);
	}

	@Override
	protected ArrayList<GoogleImage> createSearchResultList() {
		return new ArrayList<GoogleImage>();
	}

	@Override
	protected Request<GoogleImageSearchResponse> createRequest(final String query, Object data) {
		final int page = (data != null) ? (Integer)data : 0;
		String url;
		try {
			url = String.format(SEARCH_URL, 8 * page, URLEncoder.encode(query, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			return null;
		}
		
		GsonRequest<GoogleImageSearchResponse> request = new GsonRequest<GoogleImageSearchResponse>(
			Request.Method.GET,
			url, GoogleImageSearchResponse.class, null,
			new Response.Listener<GoogleImageSearchResponse>()
			{
				@Override
				public void onResponse(GoogleImageSearchResponse response)
				{
					if (response.responseData != null && response.responseData.results != null && response.responseData.results.length > 0)
					{
						for (GoogleImage image : response.responseData.results)
						{
							searchResult.add(image);
						}

						if (page < 6)
						{
							search(query, page + 1);
						}
						else
						{
							notifySearchDone();
							notifyDataSetChanged();
						}
					}
					else
					{
						notifySearchDone();
					}
				}
			},
			new VolleyErrorResponseListener()
		);
		
		return request;
	}

	@Override
	public ImageData getItem(int position)
	{
		GoogleImage result = searchResult.get(position);
		return new ImageData(result.titleNoFormatting + "<p/>" + result.contentNoFormatting, result.tbUrl, result.url);
	}

}
